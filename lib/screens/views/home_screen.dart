import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:news_app/models/article.dart';
import 'package:news_app/models/categorie.dart';
import 'package:news_app/screens/widgets/actu_banner.dart';
import 'package:news_app/screens/widgets/article_filtre_widget.dart';
import 'package:news_app/screens/widgets/article_widget.dart';
import 'package:news_app/services/news.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _myController = TextEditingController();
  final _myControllerCategorie = TextEditingController();

  late bool _isSearch = false;
  late bool _isLoading = true;
  late bool isAccueil = true;
  int? categorieColor;

  late List<Article> articles = <Article>[];
  late List<Article> actu = <Article>[];
  late List<Article> articlesFiltre = <Article>[];
  late List<Categorie> categorie = <Categorie>[];
  Future<void> getNews() async {
    News news = News();
    await news.getNews();
    await news.getActu();
    await news.getCategorie();
    categorie = news.categorie;
    articles = news.news;
    actu = news.actu;
    setState(() {
      articlesFiltre = articles;
      _isLoading = false;
    });
  }

  @override
  void initState() {
    getNews();
    super.initState();
    _isLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.dark,
        centerTitle: true,
        elevation: 0,
        title: _isLoading
            ? Container()
            : _isSearch
                ? Row(
                    children: [
                      Flexible(
                        child: SizedBox(
                          height: 50,
                          child: TextFormField(
                            textAlign: TextAlign.start,
                            style: const TextStyle(
                              fontSize: 20.0,
                            ),
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                errorStyle: const TextStyle(
                                    fontFamily: 'Montserrat', fontSize: 12.0),
                                hintStyle: const TextStyle(
                                  fontSize: 18,
                                ),
                                suffixIcon: _myController.text.isNotEmpty
                                    ? IconButton(
                                        onPressed: () {
                                          setState(() {
                                            _myController.clear();
                                          });
                                        },
                                        icon: const Icon(Icons.cancel))
                                    : Container(
                                        width: 1,
                                      ),
                                prefixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _myController.clear();
                                      _myControllerCategorie.clear();
                                      _isSearch = false;
                                    });
                                  },
                                  icon: const Icon(Icons.arrow_back_ios),
                                  color: Colors.black,
                                ),
                                fillColor: Colors.grey.shade100,
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide.none),
                                hintText: 'Rechercher'),
                            controller: _myController,
                            onChanged: (article) {
                              setState(() {
                                articlesFiltre = articles
                                    .where(
                                      (element) =>
                                          element.titre.toLowerCase().contains(
                                                article.toLowerCase(),
                                              ),
                                    )
                                    .toList();
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  )
                : const Text(
                    "News App",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.black,
                        fontFamily: "Montserrat",
                        fontWeight: FontWeight.bold),
                  ),
        actions: [
          _isLoading
              ? Container()
              : _isSearch
                  ? Container()
                  : IconButton(
                      onPressed: () {
                        setState(() {
                          _isSearch = true;
                        });
                      },
                      icon: const Icon(
                        Icons.search,
                        color: Colors.black,
                      ),
                    ),
        ],
        backgroundColor: Colors.white,
      ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : _myController.text.isEmpty
              ? Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.black12.withOpacity(0.01)),
                      height: 50,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isAccueil = true;
                                _myController.clear();
                                _myControllerCategorie.clear();
                                _isSearch = false;
                              });
                            },
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              child: const Center(
                                child: Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    "Accueil",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "Montserrat"),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: categorie.length,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isAccueil = false;
                                      categorieColor = index;
                                      _myControllerCategorie.text =
                                          categorie[index].nomCategorie;
                                      articlesFiltre = articles
                                          .where(
                                            (element) => element.categorie
                                                .toLowerCase()
                                                .contains(categorie[index]
                                                    .nomCategorie
                                                    .toLowerCase()),
                                          )
                                          .toList();
                                    });
                                  },
                                  child: Card(
                                    color: isAccueil
                                        ? Colors.white
                                        : categorieColor == index
                                            ? Colors.blue
                                            : Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5)),
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          categorie[index].nomCategorie,
                                          style: TextStyle(
                                              color: isAccueil
                                                  ? Colors.black
                                                  : categorieColor == index
                                                      ? Colors.white
                                                      : Colors.black,
                                              fontWeight: FontWeight.w400,
                                              fontFamily: "Montserrat"),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ],
                      ),
                    ),
                    _myControllerCategorie.text.isEmpty
                        ? Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Column(
                              children: [
                                Text(
                                  "à la une".toUpperCase(),
                                  style: const TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500),
                                ),
                                ActuBanner(actu: actu)
                              ],
                            ),
                          )
                        : Flexible(
                            child: ArticleFiltreWidget(
                                articlesFiltre: articlesFiltre)),
                    _myControllerCategorie.text.isEmpty
                        ? Flexible(
                            child: ArticleWidget(articles: articles),
                          )
                        : Container()
                  ],
                )
              : SizedBox(
                  height: height,
                  child: ArticleFiltreWidget(articlesFiltre: articlesFiltre),
                ),
    );
  }
}
