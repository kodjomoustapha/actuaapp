import 'package:flutter/material.dart';
import 'package:news_app/screens/views/article_screen.dart';

class ArticleWidget extends StatelessWidget {
  final List articles;
  const ArticleWidget({
    Key? key,
    required this.articles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListView.builder(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        scrollDirection: Axis.vertical,
        itemCount: articles.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ArticleScreen(
                  article: articles[index],
                ),
              ),
            ),
            child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Image.asset(
                          articles[index].img,
                          fit: BoxFit.fill,
                          height: width * 0.15,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        SizedBox(
                            width: width * 0.65,
                            child: Text(
                              articles[index].titre,
                              style: const TextStyle(
                                  fontSize: 13, fontFamily: "MontSerrat"),
                            )),
                        const SizedBox(
                          height: 2,
                        ),
                        SizedBox(
                          width: width * 0.65,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                articles[index].categorie,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                )),
          );
        });
  }
}
