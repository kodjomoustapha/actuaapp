import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:news_app/screens/views/article_screen.dart';

class ActuBanner extends StatelessWidget {
  final List actu;
  const ActuBanner({Key? key, required this.actu}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      itemCount: actu.length,
      itemBuilder: (BuildContext context, int index, int pageViewIndex) =>
          CardActu(actu: actu, index: index),
      options: CarouselOptions(
        autoPlay: true,
        enlargeCenterPage: true,
        viewportFraction: 0.6,
        aspectRatio: 1.5,
        initialPage: 2,
      ),
    );
  }
}

class CardActu extends StatelessWidget {
  final List actu;
  final int index;
  const CardActu({Key? key, required this.actu, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ArticleScreen(
            article: actu[index],
          ),
        ),
      ),
      child: SizedBox(
        width: 400,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.asset(
                    actu[index].img,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.55,
                    // height: 50,
                    child: Text(
                      actu[index].titre,
                      textAlign: TextAlign.justify,
                      style: const TextStyle(
                        fontSize: 12,
                        fontFamily: "Montserrat",
                        color: Colors.black87,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
