import 'package:flutter/material.dart';
import 'package:news_app/screens/views/article_screen.dart';

class ArticleFiltreWidget extends StatelessWidget {
  final List articlesFiltre;
  const ArticleFiltreWidget({
    Key? key,
    required this.articlesFiltre,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return ListView.builder(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        scrollDirection: Axis.vertical,
        itemCount: articlesFiltre.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ArticleScreen(
                  article: articlesFiltre[index],
                ),
              ),
            ),
            child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Image.asset(
                          articlesFiltre[index].img,
                          fit: BoxFit.fill,
                          height: width * 0.15,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        SizedBox(
                            width: width * 0.65,
                            child: Text(
                              articlesFiltre[index].titre,
                              style: const TextStyle(
                                  fontSize: 13, fontFamily: "MontSerrat"),
                            )),
                        const SizedBox(
                          height: 2,
                        ),
                        SizedBox(
                          width: width * 0.65,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                articlesFiltre[index].categorie,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                )),
          );
        });
  }
}
