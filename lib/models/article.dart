class Article {
  late final String auteur;
  late final String titre;
  late final String description;
  late final String img;
  late final String categorie;
  late final String datePub;

  Article({
    required this.auteur,
    required this.titre,
    required this.description,
    required this.img,
    required this.categorie,
    required this.datePub,
  });
}
