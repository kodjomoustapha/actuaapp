import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:news_app/models/article.dart';
import 'package:news_app/models/categorie.dart';

class News {
  List<Article> news = [];
  List<Article> actu = [];
  List<Categorie> categorie = [];
  Future<void> getNews() async {
    final String response = await rootBundle.loadString("assets/articles.json");
    var jsonData = jsonDecode(response);
    jsonData['articles'].forEach((element) {
      news.add(
        Article(
            auteur: element["auteur"],
            titre: element["titre"],
            description: element["description"],
            img: element["img"],
            categorie: element["categorie"],
            datePub: element["datePub"]),
      );
    });
  }

  Future<void> getActu() async {
    final String response = await rootBundle.loadString("assets/articles.json");
    var jsonData = jsonDecode(response);
    jsonData['actualite'].forEach((element) {
      actu.add(
        Article(
            auteur: element["auteur"],
            titre: element["titre"],
            description: element["description"],
            img: element["img"],
            categorie: element["categorie"],
            datePub: element["datePub"]),
      );
    });
  }

  Future<void> getCategorie() async {
    final String response = await rootBundle.loadString("assets/articles.json");
    var jsonData = jsonDecode(response);
    jsonData['categorie'].forEach((element) {
      categorie.add(Categorie(nomCategorie: element['nomCategorie']));
    });
  }
}
